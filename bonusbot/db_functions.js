const co = require('co');
const mysql = require('mysql2');
const { MYSQL_PASSWORD } = require('./consts_loc.js');

const pool = mysql.createPool({
  connectionLimit : 10,
  host: 'mysql',
  user: 'root',
  password: MYSQL_PASSWORD,
  database: 'bonustat',
});

function getTotalInfo(lot) {
  return co.wrap(function*() {
    let message = `Завершился лот ${lot.description}. Победителем лота стал #`;
    const query = `SELECT p, (SELECT nick FROM users WHERE bids.user_id = users.id) as user FROM bids WHERE lot_id=${lot.lot_id} ORDER BY t DESC LIMIT 2`;
    const result = yield run(query);
    if(result.length) {
      message += `${result[0].user}. Товар обойдется ему в ${result[0].p} руб.\n`;
      message += 'Теперь немного статистики на количество ставок:\n';

      const query2 = `SELECT (SELECT nick FROM users WHERE bids.user_id = users.id) as user, count(user_id) as bids_count FROM bids WHERE lot_id=${lot.lot_id} GROUP BY user_id ORDER BY bids_count DESC`;
      const result2 = yield run(query2);
      if(result2.length) {
        const winner = result2.find(r => r.user == result[0].user);
        message += `1. Победитель сделал ставок - ${winner.bids_count}\n`;
        message += `2. Больше всех ставок сделал #${result2[0].user} - ${result2[0].bids_count}\n`;

        const last_rival = result2.find(r => r.user == result[1].user);
        message += `3. Соперник победителя под ником #${result[1].user} - ${last_rival.bids_count}\n`;
        message += 'Список игроков, сделавших больше 100 ставок:\n';

        result2.forEach(res => {
          if(res.bids_count >= 100) message += `#${res.user} ${res.bids_count}\n`;
        });

        message += '\n#bonusmall #бонусмол\n';

        const query3 = `SELECT url FROM goods WHERE id=(SELECT good_id FROM lots_history WHERE lot_id=${lot.lot_id})`;
        const result3 = yield run(query3);
        message += `bonusmall.ru/product/${result3[0].url}`;

        return message;
      } else return `Can not get bids count for lot ${lot.lot_id}: ${lot.description}`;
  } else return `There are no total information about lot ${lot.lot_id}: ${lot.description}`;
  })();
}

module.exports = {
  run,
  getTotalInfo
}

function run(sql) {
  return new Promise((res, rej) => {
    pool.getConnection((err, connection) => {
      connection.query(sql, (err, results) => {
        connection.release();
        if(err) return rej(err);

        res(results);
      });
    });
  });
}