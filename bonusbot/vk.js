const https = require('https');
const fs = require('fs');
const path = require('path');
const url = require('url');
const { appId, appSecret, accessToken, groupId } = require('./consts_loc.js');
const VK = require('vksdk');

const vk = new VK({
  appId: appId,
  appSecret: appSecret,
  secure: true,
});

async function vkPublicPost(text, imgsPaths) {
  vk.setToken(accessToken);
  const data = await request('photos.getWallUploadServer', { group_id: groupId });
  if(data.response && data.response.upload_url) {
    let ownerId;

    const imgsIds = [];
    for(let i = 0; i < imgsPaths.length; i++) {
      const path = imgsPaths[i];

      const rawUploadingResponse = await uploadImage(data.response.upload_url, path);

      try {
        const { server, photo, hash } = JSON.parse(rawUploadingResponse);
      
        const savedPhoto = await request('photos.saveWallPhoto', { server, photo, hash, group_id: groupId });

        if(savedPhoto.response) {
          console.log(savedPhoto.response);
          ownerId = savedPhoto.response[0].owner_id;
          imgsIds.push(savedPhoto.response[0].id);
        }
        else throw new Error('method photos.saveWallPhoto return error: ' + JSON.stringify(savedPhoto));
      } catch(err) {
        throw err;
      }
    }

    const attachments = imgsIds.map(img => `photo${ownerId}_${img}`);
    const wallPost = await request('wall.post', { owner_id: -groupId, from_group: 1, message: text, attachments: attachments.join(',') });
  } else throw new Error('error on get upload server');
}

module.exports = {
  vkPublicPost,
}

vk.on('http-error', function(_e) {
  console.log('http-error: ' + _e);
});

vk.on('parse-error', function(_e) {
  console.log('parse-error: ' + _e);
});

function request(_method, _params) {
  return new Promise(res => {
    vk.request(_method, _params, response => res(response));
  });
}

function uploadImage(upload_url, filepath) {
  return new Promise(res => {
    const upload = url.parse(upload_url);
    const options = {
      host: upload.host,
      path: upload.path,
      method: 'POST',
    };
    let summary = '';
    const req = https.request(options, function(response) {
      response.setEncoding('utf8');
      response.on('data', function (chunk) {
        summary += chunk;
      });
      response.on('end', function() {
        res(summary);
      });
    });
    const data = fs.readFileSync(filepath);
    const crlf = '\r\n';
    const boundary = Math.random().toString(16);
    const delimiter = `${crlf}--${boundary}`;
    const headers = [
      `Content-Disposition: form-data; name="photo"; filename=${path.basename(filepath)}` + crlf
    ];
    const closeDelimiter = `${delimiter}--`;

    const multipartBody = Buffer.concat([
      new Buffer(delimiter + crlf + headers.join('') + crlf),
      data,
      new Buffer(closeDelimiter),
    ]);

    req.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
    req.setHeader('Content-Length', multipartBody.length);

    req.write(multipartBody);
    req.end();
  });
}