const co = require('co');
const { run, getTotalInfo } = require('./db_functions.js');
const { downloadImages } = require('./download_images.js');
const { vkPublicPost } = require('./vk.js');
const got = require('got');
const cheerio = require('cheerio');

module.exports = {
  sub(bot, msg, match, subLots) {
    const fromId = msg.from.id;
    const lotIds = match[1].split(' ').filter(id => !!id);

    lotIds.forEach(l => subLots[l] = { id: l });
    const message = 'You have subscribed successfully on this lot(s)!';

    bot.sendMessage(fromId, message);
  },
  unsub(bot, msg, match, subLots) {
    const fromId = msg.from.id;
    const lotIds = match[1].split(' ').filter(id => !!id);

    lotIds.forEach(l => { if(subLots[l]) delete subLots[l] });
    const message = 'You have unsubscribed successfully on this lot(s)!';

    bot.sendMessage(fromId, message);
  },
  showSubs(bot, msg, match, subLots) {
    const fromId = msg.from.id;
    let message = '';
    for(let key in subLots) {
      message += `${subLots[key].id}: ${subLots[key].description}\n` 
    }
    bot.sendMessage(fromId, message);
  },
  async public(bot, msg, match) {
    const fromId = msg.from.id;
    const lotId = match[1];
    try {
      const lot = await run(`SELECT * FROM lots_history, goods WHERE lot_id=${lotId} AND lots_history.good_id=goods.id`);

      const postText = await getTotalInfo(lot[0]);
      console.log('get postText');
      const imgsPaths = await getDownloadedGoodImagesPaths(lot[0].url);
      console.log('get imgsPaths', imgsPaths);

      const result = await publicPost(postText, imgsPaths);
      console.log('send post', result);

      bot.sendMessage(fromId, result);
    } catch(err) {
      bot.sendMessage(fromId, err.message);
    }
  }
}

async function publicPost(postText, imgsPaths) {
  try {
    await vkPublicPost(postText, imgsPaths);
  } catch(err) {
    throw err;
  }
  return 'Post was published successfully.';
}

async function getGoodImagesUrls(url) {
  const imgs = [];
  const goodUrl = 'https://bonusmall.ru/product/' + url;

  try {
    const headers = {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
      'Accept-Encoding': 'gzip, deflate, br',
      'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,bg;q=0.2',
      'Cache-Control': 'max-age=0',
      'Connection': 'keep-alive',
      'Host': 'bonusmall.ru',
      'Upgrade-Insecure-Requests': 1,
      'User-Agent': 'Mozilla/5.0 (Windows NT; x64; rv:47.0) Gecko/20100101 Firefox/47.0 Chrome/53.0.2764.0 Safari/537.36 YaBrowser/1.20.1364.172 Amigo/32.0.1717.129',
      'X-Compress': null,
    }
    const res = await got(goodUrl, { timeout: 5000, headers });
    const body = res.body;

    if(/403 Forbidden/.test(body)) throw new Error('Forbidden error');

    const $ = cheerio.load(body);
    $('.picture-item').each((i, el) => {
      const src = $(el).children('img').attr('src');
      imgs.push('https://bonusmall.ru' + src);
    });
  } catch(err) {
    throw err;
  }

  return imgs;
}

async function getDownloadedGoodImagesPaths(url) {
  try {
    const imgsUrls = await getGoodImagesUrls(url);
    const imgsPaths = await downloadImages(imgsUrls);

    return imgsPaths;
  } catch(err) {
    throw err;
  }
}