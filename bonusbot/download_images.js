const https = require('https');
const path = require('path');
const fs = require('fs');

async function downloadImages(imgsUrls) {
  const imgsPaths = [];
  for(let i = 0; i < imgsUrls.length; i++) {
    try {
      const imgPath = __dirname + '/images/img' + i + '.png';
      await getAndWrite(imgsUrls[i], imgPath);

      imgsPaths.push(imgPath);
    } catch(err) {
      throw err;
    }
  }

  return imgsPaths;
}

module.exports = {
  downloadImages
}

function getAndWrite(url, imgPath) {
  return new Promise((res, rej) => {
    const options = {
      host: url,
      family: 4,
      headers: {
        'Host': 'bonusmall.ru',
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36',
        'Accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
        'Referer': 'https://bonusmall.ru/',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,bg;q=0.2',
      }
    }
    https.get(url, (result) => {
      let imgData = '';
      result.setEncoding('binary');
      result.on('data', (chunk) => {
        imgData += chunk;
      });

      result.on('end', () => {
        if(/html/.test(imgData)) {
          console.log('Forbidden error', imgData);
          rej('Forbidden error');
        }

        fs.writeFileSync(imgPath, imgData, { encoding: 'binary', flag: 'w' });
        res();
      });
    });
  })
}