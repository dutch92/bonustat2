const co = require('co');
const Bot = require('node-telegram-bot-api');
const { run, getTotalInfo } = require('./db_functions.js');
const bF = require('./bonusbot_functions.js');
const settings = require('./consts_loc.js');
const bot = new Bot(settings.token, { polling: true });

const subLots = {};
let chatId = settings.chatId;

co(function*() {
  while(true) {
    const lotsIds = Object.keys(subLots);

    if(lotsIds.length) {
      const inLotsCondition = lotsIds.join(',');
      const lotsFromDb = yield run(`SELECT lot_id, status, (SELECT description FROM goods WHERE lots_history.good_id=goods.id) as description FROM lots_history WHERE lot_id IN (${inLotsCondition})`);

      if(lotsFromDb.length) {
        for(let i = 0; i < lotsFromDb.length; i++) {
          const l = lotsFromDb[i];

          if(subLots[l.lot_id].status === undefined) {
            subLots[l.lot_id].status = l.status;
            subLots[l.lot_id].description = l.description;

            bot.sendMessage(chatId, `Add status ${l.status} to ${l.description}`);
          } else if(subLots[l.lot_id].status != l.status) {
            subLots[l.lot_id].status = l.status;
            let message = `Lot ${l.lot_id}: ${l.description} has changed status to ${l.status}\n`;
            if(l.status == 1) {
              message += yield getTotalInfo(l);
            }

            bot.sendMessage(chatId, message);

            if(l.status == 1) delete subLots[l.lot_id];
          }
        }
      }
    }

    yield cb => setTimeout(cb, 10000);
  }
}).catch(err => console.error('ERROR', err, Date.now()));

bot.on('message', (msg) => {
  if(!chatId) chatId = msg.from.id;

  regexpCommands.some(r => {
    const match = msg.text.match(r.regexp);

    if(!match) return;
    
    r.foo(bot, msg, match, subLots);
    
    return true;
  });
});

const regexpCommands = [
  { regexp: /\/sub((?: \d{6})+)/, foo: bF.sub },
  { regexp: /\/unsub((?: \d{6})+)/, foo: bF.unsub },
  { regexp: /^\/showSubs$/, foo: bF.showSubs },
  { regexp: /\/public (\d{6})/, foo: bF.public },
]