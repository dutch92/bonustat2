1. Create volume `docker volume create bmdata`
2. Run base services: `docker-compose up -d mysql phpmyadmin`
3. Run services: `sh bin/start.sh [-d] [grabber | bonusbot]`