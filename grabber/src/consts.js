module.exports = {
  'STATUS_COMPLETED': 1,
  'STATUS_ACTIVE': 2,
  'STATUS_FUTURE': 3,
  userAgent: 'Mozilla/5.0 (Windows NT; x64; rv:47.0) Gecko/20100101 Firefox/47.0 Chrome/53.0.2764.0 Safari/537.36 YaBrowser/1.20.1364.172 Amigo/32.0.1717.129',
  ajaxHeaders: {
    'Connection': 'keep-alive',
    'Origin': 'https://bonusmall.ru',
    'Referer': 'https://bonusmall.ru/',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'X-Requested-With': 'XMLHttpRequest',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,bg;q=0.2',
    'X-Compress': null
  }
}