const mysql = require('mysql2');
const { MYSQL_PASSWORD } = require('./consts_loc.js');
const consts = require('./consts.js');
const co = require('co');
const got = require('got');
const cheerio = require('cheerio');

const pool = mysql.createPool({
  connectionLimit : 10,
  host: 'mysql',
  user: 'root',
  password: MYSQL_PASSWORD,
  database: 'bonustat',
});
const cookie = {};

co(function*() {
  yield createTabels();

  const cleaner = new Cleaner();
  const goods = {};
  const users = {};
  let lots = yield getOriginLots();

  yield updateLotsHistory(lots, goods);

  let lastTime = Date.now() / 1000 - 5;
  let execTime = 0;

  while(true) {
    if(cleaner.canStartClean()) yield cleaner.clean();
    try {
      if(execTime / 1000 > 20 ) {
        execTime = 0;

        clearUsers(users);
        clearGoods(goods);

        lots = yield getOriginLots();

        yield updateLotsHistory(lots, goods);
      }
    } catch(err) {
      console.error(err, 'WHEN EXECTIME > 20 sec', Date.now());
    }

    const start = Date.now();
    try {
      const form = {};
      lots.active.forEach((lot, i) => {
        form[`lotlist[${i}][saleid]`] = lot.id;
        form[`lotlist[${i}][rendertype]`] = 'main';
      });
      const headers = Object.assign({}, consts.ajaxHeaders);
      const prepareForm = encodeURIComponent(Object.keys(form).map(key => `${key}=${form[key]}`).join('&'));
      const contentLength = prepareForm.length;
      headers['User-Agent'] = consts.userAgent;
      headers['Cookie'] = prepareCookie(cookie);

      const response = yield got.post('https://bonusmall.ru/ajax.php?action=salestate&changeid=' + lastTime, { body: form, timeout: 5000, headers, form: true });

      if(response.statusCode == 403) throw new Error('ERROR FORBIDDEN');

      updateCookie(cookie, response.headers['set-cookie']);

      const stats = JSON.parse(response.body);

      for(let key in stats.sales) {
        if(stats.sales[key].events) {
          const events = stats.sales[key].events;

          for (let i = 0; i < events.length; i++) {
            try {
              const e = events[i];
              switch(e.changetype) {
                case 1: {
                  let user = users[e.nick];
                  if(!user) {
                    const userInDB = yield run(`SELECT id, nick FROM users WHERE nick="${e.nick}"`);

                    if(userInDB[0]) user = { id: userInDB[0].id, time: getNow() };
                    else {
                      const result = yield run(`INSERT INTO users (nick, createdAt) VALUES ("${e.nick}", ${getNow()})`);
                      user = {
                        id: result.insertId,
                        time: getNow()
                      }
                    } 

                    users[e.nick] = user;
                  }
                  const price = parseFloat(e.price.replace(',','.').replace(' ',''));
                  try {
                    yield run(`INSERT INTO bids (lot_id, t, user_id, p) VALUES (${key}, ${e.bidtm + (60 * 60 * 5)}, ${user.id}, ${price})`);
                  } catch(err) {
                    console.log(`${key} - ${price} is already in DB`);
                  }

                  break;
                }
                case 2: {
                  const completedLot = lots.active.find(l => l.id == key);

                  if(completedLot) {
                    const userIdQuery = `SELECT user_id FROM bids WHERE lot_id=${completedLot.id} ORDER BY t DESC LIMIT 1`;
                    const winner = yield run(`SELECT * FROM users WHERE id=(${userIdQuery})`);

                    console.log(`${getTime()}: Lot #${key}: ${completedLot.description} has been completed. Winner: ${(winner[0] ? winner[0].nick : '__NOUSER__')}`);
                  }

                  break;
                }
                default: {
                  console.log('Another changetype: ' + e.changetype, 'Result is: ' + result);
                  break;
                }
              }
            } catch(err) {
              console.error(err, 'ERROR IN EVENTS');
            }
          }
        }
      }
      
      lastTime = stats.changeid;
    } catch(err) {
      console.error(err, 'MAIN CYCLE ERROR', Date.now());
    }
    yield cb => setTimeout(cb, 3500);
    const end = Date.now();
    execTime += end - start;
  }
}).catch(err => console.error('ERROR', err, Date.now()));

function getOriginLots() {
  return co.wrap(function*() {
    const lots = {
      active: [],
      completed: [],
      future: []
    };
    const headers = getHeaders(cookie);
    const res1 = got('https://bonusmall.ru/auction/1', { headers, timeout: 5000 });
    const res2 = got('https://bonusmall.ru/auction/2', { headers, timeout: 5000 });

    const result = yield Promise.all([res1, res2]);

    result.forEach(res => {
      const body = res.body;

      if(res.statusCode == 403) throw new Error('Forbidden error');

      updateCookie(cookie, res.headers['set-cookie']);

      const $ = cheerio.load(body);
      const activeLots = getLotsAsArray($, 'active');
      const completedLots = getLotsAsArray($, 'completed');
      const futureLots = getLotsAsArray($, 'future');

      lots.active = lots.active.concat(activeLots);
      lots.completed = lots.completed.concat(completedLots);
      lots.future = lots.future.concat(futureLots);
    });

    return lots;
  })();
}

function updateLotsHistory(lots, goods) {
  return co.wrap(function*() {
    const dbActive = yield run(`SELECT * FROM lots_history WHERE status=${consts.STATUS_ACTIVE}`);
    const dbFuture = yield run(`SELECT * FROM lots_history WHERE status=${consts.STATUS_FUTURE}`);
    const dbCompleted = yield run(`SELECT * FROM lots_history WHERE status=${consts.STATUS_COMPLETED}`);

    for(let i = 0; i < lots.future.length; i++) {
      const l = lots.future[i];
      const index = dbFuture.findIndex(item => item.lot_id == l.id);

      if(index > -1) dbFuture.splice(index, 1);
      else {
        const good = yield getGood(l, goods);
        console.log(`${getTime()}: trying to insert NEW lot ${l.id}: ${l.description} set FUTURE`);
        try {
          yield run(`INSERT INTO lots_history (lot_id, good_id, status) VALUES (${l.id}, ${good.id}, ${consts.STATUS_FUTURE})`);
        } catch(err) {
          console.log(`${getTime()}:`, err);
        }
      }
    }

    for(let i = 0; i < lots.active.length; i++) {
      const l = lots.active[i];
      const index = dbActive.findIndex(item => item.lot_id == l.id);

      if(index > -1) dbActive.splice(index, 1);
      else {
        const futureIndex = dbFuture.findIndex(f => f.lot_id == l.id);

        if(futureIndex > -1) {
          console.log(`${getTime()}: trying to update lot ${l.id}: ${l.description} set ACTIVE`);
          dbFuture.splice(futureIndex, 1);
          try {
            yield run(`UPDATE lots_history SET status=${consts.STATUS_ACTIVE} WHERE lot_id=${l.id}`);
          } catch(err) {
            console.log(`${getTime()}:`, err);
          }
        } else {
          const good = yield getGood(l, goods);
          console.log(`${getTime()}: trying to insert NEW lot ${l.id}: ${l.description} set ACTIVE`);
          try {
            yield run(`INSERT INTO lots_history (lot_id, good_id, status) VALUES (${l.id}, ${good.id}, ${consts.STATUS_ACTIVE})`);
          } catch(err) {
            console.log(`${getTime()}:`, err);
          }
        }
      }
    }

    if(dbActive.length) {
      for(let i = 0; i < dbActive.length; i++) {
        const l = dbActive[i];
        const lastBid = yield run(`SELECT user_id FROM bids WHERE lot_id=${l.lot_id} ORDER BY t DESC LIMIT 1`);
        console.log(`${getTime()}: trying to update lot ${l.lot_id} set COMPLETED in dbActive`);
        try {
          yield run(`UPDATE lots_history SET status=${consts.STATUS_COMPLETED}, complete_date=${getNow()}, winner_id=${(lastBid[0] ? lastBid[0].user_id : 0)} WHERE lot_id=${l.lot_id}`);
        } catch(err) {
          console.log(`${getTime()}:`, err);
        }
      }
    }
  })();
}

function getGood(lot, goods) {
  return co.wrap(function*() {
    let good = goods[lot.url];

    if(!good) {
      const goodInDB = yield run(`SELECT id, description FROM goods WHERE url="${lot.url}"`);

      if(goodInDB[0]) good = { id: goodInDB[0].id, time: getNow() };
      else {

        const result = yield run(`INSERT INTO goods (description, url) VALUES ("${lot.description}", ${lot.url})`);
        good = {
          id: result.insertId,
          time: getNow()
        };
      }

      goods[lot.url] = good;
    }

    return good;
  })();
}

function getLotsAsArray(fn, type) {
  const lotsArray = [];

  fn(`#lot_block div[name="lot${type}"]`).each((i, el) => {
    lotsArray.push({
      id: fn(el).attr('id').match(/item(\d+)/)[1],
      description: fn(el).find('.desc_text').text(),
      url: fn(el).find('.postid').attr('href').match(/product\/(\d+)/)[1]
    });
  });

  return lotsArray;
}

async function createTabels() {
  await Promise.all([
    run('CREATE TABLE IF NOT EXISTS `lots_history` ( `lot_id` INT NOT NULL , `good_id` INT NOT NULL , `status` TINYINT NOT NULL , `complete_date` INT, `winner_id` INT, UNIQUE `unique_lot` (`lot_id`) ) ENGINE = InnoDB;'),
    run('CREATE TABLE IF NOT EXISTS `goods` ( `id` INT NOT NULL AUTO_INCREMENT , `description` VARCHAR(255) NOT NULL , `url` INT NOT NULL , `image` VARCHAR(255) , PRIMARY KEY (`id`), UNIQUE `unique_good` (`url`)) ENGINE = InnoDB;'),
    run('CREATE TABLE IF NOT EXISTS `bids` ( `lot_id` INT NOT NULL , `t` INT NOT NULL , `user_id` INT NOT NULL , `p` DECIMAL(10,1) NOT NULL , UNIQUE `unique_bid` (`lot_id`, `p`), INDEX `time` (`t`)) ENGINE = InnoDB;'),
    run('CREATE TABLE IF NOT EXISTS `users` ( `id` INT NOT NULL AUTO_INCREMENT , `nick` VARCHAR(255) NOT NULL , `createdAt` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;')
  ]);
}

function run(sql) {
  return new Promise((res, rej) => {
    pool.getConnection((err, connection) => {
      connection.query(sql, (err, results) => {
        connection.release();
        if(err) return rej(err);

        res(results);
      });
    });
  });
}

function clearUsers(users) {
  if(Object.keys(users).length > 3000) {
    for(let key in users) {
      if(users[key].time + 3600 < getNow()) {
        delete users[key];
      }
    }
  }
}

function clearGoods(goods) {
  if(Object.keys(goods).length > 1000) {
    for(let key in goods) {
      if(goods[key].time + 3600 * 24 * 30 < getNow()) {
        delete goods[key];
      }
    }
  }
}

function getNow() {
  return Math.round(Date.now() / 1000);
}

function getTime() {
  const date = new Date();

  return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`; 
}

class Cleaner {
  constructor() {
    this.startTime = Date.now();
  }

  canStartClean() {
    return (Date.now() - this.startTime) > (1000 * 60 * 60 * 24);
  }

  async clean() {
    const headers = getHeaders(cookie);
    const res1 = got('bonusmall.ru/auction/1', { headers, timeout: 5000 });
    const res2 = got('bonusmall.ru/auction/2', { headers, timeout: 5000 });

    const result = await Promise.all([res1, res2]);
    let futureLots = [];

    result.forEach(res => {
      const body = res.body;

      if(res.statusCode == 403) throw new Error('Forbidden error');

      const headers = res.headers;
      updateCookie(cookie, headers['set-cookie']);

      const $ = cheerio.load(body);
      futureLots = futureLots.concat(getLotsAsArray($, 'future'));
    });

    const futureLotsIds = futureLots.map(l => l.id).join(',');

    try {
      await run(`DELETE FROM lots_history WHERE status=${consts.STATUS_FUTURE} AND lot_id NOT IN (${futureLotsIds})`);
    } catch(err) {
      console.log(err);
    }

    this.startTime = Date.now();
    console.log('successfully cleaned!');
  }
}

function getHeaders(cookie) {
  const headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,bg;q=0.2',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive',
    'Host': 'bonusmall.ru',
    'Upgrade-Insecure-Requests': 1,
    'User-Agent': consts.userAgent,
    'X-Compress': null,
  }

  if(Object.keys(cookie).length > 1) {
    headers['Cookie'] = prepareCookie(cookie);
  }

  return headers;
}

function updateCookie(cookie, set) {
  if(set) {
    set.forEach(c => {
      const mainPart = c.split(';')[0];
      const key = mainPart.split('=')[0];
      const value = mainPart.split('=')[1];
      
      cookie[key] = value;
    });
  }
}

function prepareCookie(cookie) {
  return Object.keys(cookie).map(key => `${key}=${cookie[key]}`).join(';');
}